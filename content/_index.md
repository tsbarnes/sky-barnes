---
title: "Home"
description: "social justice and open source advocate, code mom"
date: 2024-05-10T06:35:53-04:00
preview: ""
draft: false
tags: []
categories: []
type: default
menus: []
---

:wave:
hi! I'm Mx. Sky Barnes!

I'm an open source developer, mom, and social justice advocate

welcome to my personal website

[learn more about me](about/)
[check out my projects](projects/)
[see my resume](resume/)
