---
title: "About"
description: "More information about me"
date: 2024-05-10T05:29:32-04:00
preview: ""
draft: false
tags: []
categories: []
type: default
menus: ['main']
---

## About Mx. Sky Barnes

**Cyberwitch and gender anarchist.**

white/native trans non-binary Linux geek and Digimon fan.

lefthanded because even my hands are anti-authority

foss advocate and socialjustice sorceress.

*ae/aer pronouns.*
