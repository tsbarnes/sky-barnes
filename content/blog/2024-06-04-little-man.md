---
title: little man
description: "feelings about growing up"
date: 2024-06-05T01:59:19.823Z
preview: ""
draft: false
tags: []
categories: []
type: default
---
today was my son's last day of fifth grade

he's grown so much this year

I love him more than I thought it was possible for me to love someone

he's so creative too, he's a little artist

his special interest is credits remixes

I don't understand them, but he's really good at making them too

as long as they make him happy, I'm happy 💖
