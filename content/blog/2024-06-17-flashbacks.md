---
title: flashbacks
description: "potentially triggering"
date: 2024-06-17T04:16:52.126Z
preview: ""
draft: false
tags: []
categories: []
type: default
---
holy shit y'all

I had a horrible flashback earlier

I saw a meme making fun of The Washington Post, it was a screencap from Last Week Tonight, just the WP logo and the text "Shh, turn off the light. It'll be over soon" and dear Goddess who approved that?

It was extremely triggering, and set off a massive flashback

I couldn't breathe, I could feel Abe choking me again

Fortunately, my girlfriend Anna was there to support me

She helped me ground and calm down

I love her ❤️
