---
title: "GNOME Theme Settings CLI"
description: "A CLI tool for changing your GNOME theme"
date: 2024-05-23T22:49:31.529Z
preview: ""
draft: false
tags: ['gnome']
categories: []
type: default
---

## gnome-theme-settings

### Usage

```shell
theme-settings \<theme type\> \<theme name\>
```

where `theme type` can be

* gtk
* shell
* icon
* cursor

and `theme name` is the name of the theme you want to use, or blank to reset the themes to their defaults

## Links

* [GitHub repository](https://github.com/tsbarnes/gnome-theme-settings)
