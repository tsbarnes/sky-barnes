---
title: "Sync Theme"
description: "Makes libAdwaita obey your custom theme"
date: 2024-05-17T22:06:14-04:00
preview: ""
draft: false
tags: ['gnome']
categories: []
type: default
---

## sync-theme.sh

a simple shell script to make libAdwaita apps use your custom theme

## Links

* [GitLab repository](https://gitlab.gnome.org/tsbarnes/sync-theme)
