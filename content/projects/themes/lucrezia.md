---
title: "Lucrezia"
description: "A white and pink theme for GNOME and KDE"
date: 2024-05-22T17:51:23-04:00
preview: ""
draft: false
tags: ['white', 'pink']
categories: ['projects', 'themes']
type: default
---
+++
title = 'Lucrezia'
date = 2024-05-22T17:51:23-04:00
draft = false
+++

## Lucrezia, hot pink and white theme

Available for GNOME and KDE

## Links

* [GitHub repository](https://github.com/tsbarnes/Lucrezia)
