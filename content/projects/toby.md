---
title: "Toby"
description: "A virtual pet for Linux"
date: 2024-05-10T10:56:58-04:00
preview: ""
draft: false
tags: ['gnome', 'kde', 'linux', 'games']
categories: []
type: default
---

## a virtual pet for Linux

Toby is a project to create virtual pets for Linux, similar to the hardware virtual pets like Digimon

## Links

* None yet
