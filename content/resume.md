---
title: "Resume"
description: "My personal resume/CV"
date: 2024-05-10T06:20:33-04:00
preview: ""
draft: false
tags: []
categories: []
type: default
menus: ['main']
---

## Mx. Thea Sky Barnes

{{< json-resume "basics" >}}

### Work

{{< json-resume "work" >}}

### Volunteer Work

{{< json-resume "volunteer" >}}

### Education

{{< json-resume "education" >}}

### Skills

{{< json-resume "skills" >}}

### Interests

{{< json-resume "interests" >}}

### References

{{< json-resume "references" >}}
